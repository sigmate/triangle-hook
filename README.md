# Triangle Hook

This is a quite simple design of a handed triangle hook for percussion players
made with OpenSCAD.

It is supposed to support a nylon or coton thread attached from one leg to
the other, passing through the thread holes, with a terminating knot at each
end.

The design caracteristics can be adjusted dynamically by tweaking the
(hopefully) explicit variables at the beginning of the source code.

### Credits

Mathieu Demange, Vincent Tchernia : idea, design
Mathieu Demange : OpenSCAD source code