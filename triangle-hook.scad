thumbMinR = 10;                         // Thumb radius
thickness = 5;                          // Global thickness of the hook
legLength = 30;                         // Length of the legs on both sides of the thumb
angle = 5;                              // Angle of the V-shaped form
threadHoleMinD = 1;                     // Min thread hole diameter
threadHoleMaxD = 2;                     // Max thread hole diameter
threadHoleRelAngle = 20;                // Relative angle of the thread holes (0 means perpendicular to the legs)
threadHoleDistance = 10;                // Distance of thread holes from end of legs
threadHoleLength = thickness * 1.5;     // Thread holes length (should be llng enough to avoid Z-fighting when relative angle is high)

DSO = 0.01;                             // Difference Safety Offset
DDSO = DSO * 2;                         // Doubled Difference Safety Offset

// Simple module definition for a tube
module tube(height, innerDiameter, outerDiameter, fn = 10)
{

    difference()
    {

        cylinder(h = height, r = outerDiameter, $fn = fn);
        
        translate([0, 0, -0.5])
        {
            cylinder(h = height + 1, r = innerDiameter, $fn = fn);
        }
        
    }

}

union()
{

    difference()
    {

        tube(thickness, thumbMinR, thumbMinR + thickness, fn = 300);

        rotate([0, 0, angle])
        {

            translate([thumbMinR, -legLength, 0])
            {

                translate([-((thumbMinR * 2) + thickness), 0, -DSO])
                {
                    cube([(thumbMinR * 2) + (thickness * 2), legLength, thickness + DDSO]);
                }
                
            }

        }
        
        mirror([1, 0, 0])
        {

            rotate([0, 0, angle])
            {

                translate([thumbMinR, -legLength, 0])
                {

                    translate([-((thumbMinR * 2) + thickness), 0, -DSO])
                    {
                        cube([(thumbMinR * 2) + (thickness * 2), legLength, thickness + DDSO]);
                    }
                    
                }

            }

        }

    }
    
    rotate([0, 0, angle])
        {

            translate([thumbMinR, -legLength, 0])
            {

                difference()
                {
                    cube([thickness, legLength, thickness]);
                    translate([-(threadHoleLength / 2) + (thickness / 2), threadHoleDistance, (thickness / 2)])
                    {
                        rotate([0, 90, threadHoleRelAngle])
                        {
                            translate([0, 0, 0])
                            {
                                #cylinder(h = threadHoleLength + DDSO, d1 = threadHoleMinD, d2 = threadHoleMaxD, $fn = 100);
                            }
                        }
                    }
                }
            }

        }

    mirror([1, 0, 0])
    {

        rotate([0, 0, angle])
        {

            translate([thumbMinR, -legLength, 0])
            {

                difference()
                {
                    cube([thickness, legLength, thickness]);
                    translate([-(threadHoleLength / 2) + (thickness / 2), threadHoleDistance, (thickness / 2)])
                    {
                        rotate([0, 90, threadHoleRelAngle])
                        {
                            translate([0, 0, 0])
                            {
                                #cylinder(h = threadHoleLength + DDSO, d1 = threadHoleMinD, d2 = threadHoleMaxD, $fn = 100);
                            }
                        }
                    }
                }
                
            }

        }

    }

}